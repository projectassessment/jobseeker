<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Models\Candidate;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CrudController extends Controller
{
    public function index()
    {
        $candidate = Candidate::latest()->paginate(5);
        return view('/candidate', [
            'title' => 'Candidate'
        ])->with(compact(['candidate']));
    }

    public function read()
    {
        $candidate = Candidate::latest()->paginate(3);
        return view('readcandidate', [
            'title' => 'Candidate'
        ])->with(compact(['candidate']));
    }

    public function store(Request $request)
    {
        $filled = $request->validate([
            'full_name' => 'required',
            'full_name' => 'required',
            'dob' => 'required',
            'pob' => 'required',
            'gender' => 'required',
            'year_exp' => 'required',
            'last_salary' => 'required',
        ]);
        $candidate = Candidate::create($filled);
        return response()->json(['candidate' => $candidate,], 200,);
    }

    public function edit($id)
    {
        $candidate = Candidate::where('candidate_id', $id)->get();
        return response()->json(['candidate' => $candidate,], 200,);
    }

    public function save(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'dob' => 'required',
            'pob' => 'required',
            'gender' => 'required',
            'year_exp' => 'required',
            'last_salary' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/candidate')
                ->withErrors($validator)
                ->withInput();
        }
        $validated = $validator->validated();
        Candidate::where('candidate_id', $id)->update($validated);
        return response()->json(['candidate' => $validated], 200,);
    }

    public function delete($id)
    {
        $candidate = Candidate::where('candidate_id', $id)->delete();
        return response()->json(['candidate' => $candidate,], 200,);
    }

    public function search(Request $request)
    {
        $results = '';
        $candidate = Candidate::where('full_name', 'LIKE', '%' . $request->search . '%')
            ->orderBy('candidate_id', 'desc')
            ->paginate(5);
        foreach ($candidate as $data) {
            $results .= '<tr id="cand_$data->candidate_id">
                            <td>' . $data->candidate_id . '</td>
                            <td>' . $data->full_name . '</td>
                            <td>' . $data->dob . '</td>
                            <td>' . $data->pob . '</td>
                            <td>' . $data->gender . '</td>
                            <td>' . $data->year_exp . '&nbsp;years</td>
                            <td>Rp.' . number_format($data->last_salary, 0, ',', '.') . '</td>
                            <td><a onclick="edit_candidate(' . $data->candidate_id . ')"><i class="fa-solid fa-pen-to-square" style="color: var(--bs-green); cursor: pointer;" title="edit candidate" id="candidate_{{$data->candidate_id}}" data-bs-toggle="modal" data-bs-target="#formEditmodal"></i></a>
                                &nbsp;<a onclick="deleteCandidate(' . $data->candidate_id . ')"><i class="fa-solid fa-trash-can" style="color: var(--bs-red); cursor: pointer;" title="delete candidate"></i></a></td>
                        </tr>';
        }
        return response()->json(['candidate' => $results], 200,);
    }
}
