<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{
    public function getListVacancy(Request $request, string $city = null, string $position = null, string $company = null)
    {
        if ($request->isMethod('get')) {
            $response = Http::withHeaders([
                'Accept' => 'application/json',
                // 'Authorization' => 'Bearer ' . 123, // replace with your actual token
            ])->post('https://api-dev.jobseeker.app/api/v1/get-list-vacancy', [
                'limit' => '4',
                'page' => '1',
                'keyword' => $city,
                'vacancy_name' => $position,
                'employer_name' => $company,
            ]);

            $data = response($response->json())->header('Content-Type', 'application/json');
            $data = $data->getOriginalContent();
            $vancany_list = $data['data'];
            $results = '';
            foreach ($vancany_list as $vacn) {
                $min_salary = $vacn['min_salary'] / 1000000;
                $max_salary = $vacn['max_salary'] / 1000000;
                $results .= '<div class="row mb-4">
                                <div class="col s12">
                                <div class="row">
                                    <div class="col-md-8">
                                    <div class="row">
                                        <h3 style="font-weight: 700">' . $vacn['vacancy_name'] . '</h3>
                                    </div>
                                    </div>
                                    <div class="col-md-3 d-flex justify-content-end align-items-center">
                                    <div class="row">
                                        <span style="font-weight: 600; color: #20a6fc">' . $vacn['city_name'] . '</span>
                                    </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                    <div class="row mb-4">
                                        <span style="color: var(--bs-gray)">Requirements:</span>
                                    </div>
                                    <div class="row mb-3">
                                        <span style="color: var(--bs-gray)">Salary:</span>
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="row mb-1">
                                        <span style="color: var(--bs-gray); font-weight: 500">Bachelor Degree Min. 5 years of experience</span>
                                    </div>
                                    <div class="row mb-3">
                                        <span style="color: var(--bs-gray); font-weight: 500">IDR ' . $min_salary . ' - ' . $max_salary . ' Millions</span>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                    <div class="row">
                                        <span style="color: var(--bs-gray); font-style: italic">Posted 10 mins ago</span>
                                    </div>
                                    </div>
                                    <div class="col-md-3 offset-md-3 d-flex justify-content-end align-items-center" style="text-align: right">
                                    <button class="btn btn-primary" type="button" style="width: 13%; width: 90%">
                                        Get Started
                                    </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                            ';
            }

            $listCityName = [];
            foreach ($vancany_list as $vacn) {
                $listCityName[] .= $vacn['city_name'];
            }
            return response()->json(['vacancy' => $results, 'city_name' => $listCityName], 200,);
        } else {
            return response()->json(['error' => 'Method not allowed'], 405);
        }
    }

    public function getCityName(Request $request, $city)
    {
        if ($request->isMethod('get')) {
            $response = Http::withHeaders([
                'Accept' => 'application/json',
                // 'Authorization' => 'Bearer ' . 123, // replace with your actual token
            ])->post('https://api-dev.jobseeker.app/api/v1/get-list-vacancy', [
                'limit' => '4',
                'page' => '1',
                'keyword' => $city,
            ]);

            $data = response($response->json())->header('Content-Type', 'application/json');
            $data = $data->getOriginalContent();
            $cityName = $data['data'];
            return response()->json(['city_name' => $cityName], 200,);
        } else {
            return response()->json(['error' => 'Method not allowed'], 405);
        }
    }
}
