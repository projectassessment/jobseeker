<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JobController;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        "title" => "Home",
    ]);
});

Route::get('/candidate', [CrudController::class, 'index', 'read']);
Route::get('/candidate/read', [CrudController::class, 'read']);
Route::post('/candidate/create', [CrudController::class, 'store']);
Route::get('/candidate/{id}/edit', [CrudController::class, 'edit']);
Route::post('/candidate/{id}/updateprocess', [CrudController::class, 'save']);
Route::post('/candidate/{id}/delete', [CrudController::class, 'delete']);
Route::get('/candidate/search', [CrudController::class, 'search']);
Route::get('/get-list-vacancy/{city?}/{position?}/{company?}', [ApiController::class, 'getListVacancy']);
Route::get('/get-list-city/{city}', [ApiController::class, 'getCityName']);
