jQuery(document).ready(function($){
    readCandidate();
    get_vacany();
    $("#btn-save").click(function (e) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        
        e.preventDefault();

        var formData = {
            full_name: jQuery('#full_name').val(),
            dob: jQuery('#dob').val(),
            pob: jQuery('#pob').val(),
            gender: jQuery('#gender').val(),
            year_exp: jQuery('#year_exp').val(),
            last_salary: jQuery('#last_salary').val(),
        };

        var state = jQuery('#btn-save').val();
        var type = "POST";
        var candidate_id = jQuery('#candidate_id').val();
        var ajaxurl = `candidate/create`;
        
        $.ajax({
            type: type,
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function (data) {
                var data = data.candidate;
                // console.log(data);
                readCandidate();
                jQuery('#new_candidate').trigger("reset");
                jQuery('#formModal').modal('hide');
                jQuery('.modal-backdrop').hide();
                
            },
            error: function (data) {
                // console.log(data);
            }
        });  
    });

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: 'TRUE',
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        maxDate: new Date(),
    });
});

var readCandidate = () => {
    $.get('candidate/read', function(data, status){
        jQuery('#list_table_candidate').html(data);
    });
}

var edit_candidate = (id) => {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    var type = "GET";
    var ajaxurl = `candidate/${id}/edit`;

    $.ajax({
        type: type,
        url: ajaxurl,
        dataType: 'json',
        async: false,
        success: function (data) {
            var data = data.candidate[0];
            selected        =   data.gender ==  'Male' ? 'selected' : ''; 
            selected        =   data.gender ==  'Female' ? 'selected' : '';   
            var html_format =   `
                                <div class="form-group mb-2">
                                    <label>Full name</label>
                                    <input type="text" class="form-control" id="full_name_edit" name="full_name_edit" placeholder="Alex" value="${data.full_name}">
                                </div>
                                <div class="form-group date mb-2" id="datepicker">
                                    <label>Date of birth</label>
                                    <input type="text" class="form-control datepicker" id="dob_edit" value="${data.dob}" style="cursor: pointer;" />
                                    <span class="input-group-append">
                                    </span>
                                </div>
                                <div class="form-group mb-2">
                                    <label>Place of birth</label>
                                    <input type="text" class="form-control" id="pob_edit" name="pob_edit" placeholder="Jakarta" value="${data.pob}">
                                </div>
                                <div class="form-group mb-2">
                                    <label>Gender</label>
                                    <select class="form-group form-select" aria-label="Default select example" id="gender_edit">
                                        <option ${selected} value="Male">Male</option>
                                        <option ${selected} value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="form-group mb-2">
                                    <label>Years of experience</label>
                                    <input type="number" class="form-control" id="year_exp_edit" name="year_exp_edit" placeholder="5" value="${data.year_exp}">
                                </div>
                                <div class="form-group mb-2">
                                    <label>Last Salary</label>
                                    <input type="number" class="form-control" id="last_salary_edit" name="last_salary_edit" placeholder="5.000.000" value="${data.last_salary}">
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-primary" onclick="updateCandidate('${data.candidate_id}')" style="width: 30%">Save changes
                                    </a>
                                    <input type="hidden" id="candidate_id" name="candidate_id" value="0">
                                </div>
                                `;
            jQuery(`#new_candidate_edit`).html(html_format);
            jQuery('#new_candidate').trigger("reset");
        },
        error: function (data) {
            // console.log(data);
        }
    });

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: 'TRUE',
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        maxDate: new Date(),
    });
}


var updateCandidate = (id) => {
    var id = parseInt(id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    var formData = {
        full_name: jQuery('#full_name_edit').val(),
        dob: jQuery('#dob_edit').val(),
        pob: jQuery('#pob_edit').val(),
        gender: jQuery('#gender_edit').val(),
        year_exp: jQuery('#year_exp_edit').val(),
        last_salary: jQuery('#last_salary_edit').val(),
    };
    
    var type = "post";
    var ajaxurl = `candidate/${id}/updateprocess`;
    $.ajax({
        type: type,
        url: ajaxurl,
        data: formData,
        cache: false,
        success: function (data) {
            var data = data.candidate;
            // console.log(data);
            readCandidate();  
            jQuery('#formEditmodal').modal('hide');
            jQuery('.modal-backdrop').hide();
        },
        error: function (data) {
            // console.log(data);
        }
    });
}

var deleteCandidate = (id) => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    var type = "post";
    var ajaxurl = `candidate/${id}/delete`;
    $.ajax({
        type: type,
        url: ajaxurl,
        cache: false,
        success: function (data) {
            var data = data.candidate;
            // console.log(data);
            readCandidate();  
        },
        error: function (data) {
            // console.log(data);
        }
    });
}

jQuery('#search_candidate').on('keyup', function(){
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    var value = $(this).val();

    if(value == ''){
        readCandidate();
    }
    
    var type = 'GET';
    var ajaxurl = `candidate/search`;
    $.ajax({
        type: type,
        data: {'search': value},
        url: ajaxurl,
        success: function (data) {
            $('#candidate_list').html('');
            $('#candidate_list').append(data.candidate);
            // console.log(data.candidate); 

        },
        error: function (data) {
            // console.log(data);
        }
    });
})

$(document).on('click', '.pagination a', function(event){
    event.preventDefault(); 
    var page = $(this).attr('href').split('page=')[1];
    fetch_data(page);
});

var fetch_data = (page) => {
    $.ajax({
        url:`candidate/read?page=${page}`,
        success:function(data) {
            $('#list_table_candidate').html(data);
        }
    });
}

var get_vacany = () => {
    $.ajax({
        url:`get-list-vacancy`,
        success:function(data) {
            // console.log(data);
            $('#list_vacancy').html(data.vacancy);

            $.each(data.city_name, function(index, value){
                $('#list_city_name').append('');
                html_format = `
                                 <li class="list-group-item-action" onclick="get_city('${value}')" style="font-weight: 600; cursor: pointer">${value}</li>
                                `;
                $('#list_city_name').append(html_format);
            })
        }
    });
}

var get_city = (city_name) => {
    $.ajax({
        url:`get-list-vacancy/${city_name}`,
        async: false,
        success:function(data) {
            // console.log(data);
            $('#list_vacancy').html('');
            $('#list_vacancy').html(data.vacancy);
        }
    });
}

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
        callback.apply(context, args);
        }, ms || 0);
    };
}

jQuery('#search_all').keyup(delay(function (e) {
    
    var value = $(this).val();
    $.ajax({
        url:`get-list-vacancy/${value}`,
        async: true,
        success:function(data) {
            // console.log(data);
            $('#list_vacancy').html('');
            $('#list_vacancy').html(data.vacancy);
            //w
        },
    });
}, 500));
