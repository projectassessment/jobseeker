@extends('layouts.main')

@section('container')
<section style="
        display: grid;
        width: 100%;
        height: auto;
        background: var(--bs-white);
        gap: 20px;
        margin-top: 5rem;
      ">
  <div class="container-xl">
    <div class="row">
      <div class="col-md-6 align-self-center d-md-block p-3">
        <div class="row gx-0 mb-4">
          <h1 style="font-weight: 400">We help you</h1>
          <h1 class="fw-bold">build your job portal</h1>
        </div>
        <div class="row">
          <p>
            Why use third party when you can build your own,<br />
            customizable job portal for your specific requirements
          </p>
        </div>
      </div>
      <div class="col-md-6 align-self-center d-md-block">
        <img src="./assets/img_1.svg" class="img-fluid" style="width: 678.5px; height: 498.5px; object-fit: none" />
      </div>
    </div>
    <div class="row" style="margin-top: 8rem">
      <div class="col-md-12 text-center">
        <div class="row">
          <h1 style="font-weight: 400">
            Powered by
            <br />Applicant Tracking System
          </h1>
        </div>
        <div class="row mt-5 mb-5">
          <p>
            Applicant Tracking System or ATS is software that streamlines
            and automates the end-to-end recruitment process for an employer
            or recruiter from source to hire. 99% of Fortune 500 companies
            use ATS to make their hiring process faster, cheaper and more
            effective
          </p>
        </div>
        <div class="row d-block">
          <button class="btn btn-primary" type="button" style="width: 13%">
            Get Started
          </button>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="text-center" style="margin-top: 8rem">
  <div
    class=" p-5"
    style="
      background-image: url('./assets/gradient.svg');
      max-width: 100%;
      height: 250px;
    "
  >
    <div class="row mb-5">
      <h1 style="font-weight: 400; color: var(--bs-white)">
        Ready to build your own job portal?
      </h1>
    </div>
    <div class="row d-block">
      <button class="btn btn-primary" type="button" style="width: 13%">
        Get Started
      </button>
    </div>
  </div>
</section>
<section style="
        display: grid;
        width: 100%;
        height: auto;
        background: var(--bs-white);
        gap: 20px;
      ">
  <div class="container-xl">
    <div class="row mt-5">
      <div class="col-md-5 offset-md-1 align-self-center d-md-block">
        <img src="./assets/person_1.svg" class="img-fluid" style="width: 460px; height: 498.5px; object-fit: contain" />
      </div>
      <div class="col-md-5 align-self-center d-md-block p-3">
        <div class="row gx-0 mb-4">
          <h4 style="font-weight: 400">High Quality Candidates</h4>
          <span style="color: var(--bs-gray); font-size: 14pt">Get more high quality candidates faster and at lower cost per
            hire</span>
        </div>
        <div class="row">
          <a class="fw-bold" style="
                  color: var(--color_1);
                  text-decoration: none;
                  cursor: pointer;
                ">Get Started&nbsp;<i class="fa-solid fa-arrow-right"></i></a>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-5 offset-md-1 align-self-center d-md-block p-3">
        <div class="row gx-0 mb-4">
          <h4 style="font-weight: 400">Saved Talent Pool</h4>
          <span style="color: var(--bs-gray); font-size: 14pt">Boost employer brand and build your own talent pool</span>
        </div>
        <div class="row">
          <a class="fw-bold" style="
                  color: var(--color_1);
                  text-decoration: none;
                  cursor: pointer;
                ">Get Started&nbsp;<i class="fa-solid fa-arrow-right"></i></a>
        </div>
      </div>
      <div class="col-md-5 align-self-center d-md-block">
        <img src="./assets/person_1.svg" class="img-fluid" style="width: 460px; height: 498.5px; object-fit: contain" />
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-5 offset-md-1 align-self-center d-md-block">
        <img src="./assets/person_3.svg" class="img-fluid" style="width: 460px; height: 498.5px; object-fit: contain" />
      </div>
      <div class="col-md-5 align-self-center d-md-block p-3">
        <div class="row gx-0 mb-4">
          <h4 style="font-weight: 400">
            Automated hiring Process & Insights
          </h4>
          <span style="color: var(--bs-gray); font-size: 14pt">Leverage insights from automated hiring process to win the war
            for talent</span>
        </div>
        <div class="row">
          <a class="fw-bold" style="
                  color: var(--color_1);
                  text-decoration: none;
                  cursor: pointer;
                ">Get Started&nbsp;<i class="fa-solid fa-arrow-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="mb-5" style="
        display: grid;
        width: 100%;
        height: auto;
        background: var(--bs-white);
        gap: 20px;
      ">
  <div class="container-xl">
    <div class="row mt-5">
      <div class="col-md-6 offset-md-3 text-center">
        <h1 class="mb-4" style="font-weight: 400">
          Search any jobs for You!
        </h1>
        <div class="input-group" style="
                box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1);
                border-radius: 50px;
              ">
          <input class="form-control border rounded-pill" placeholder="Position, City or Company Name" id="search_all" type="search" value="" style="
                  height: 70px;
                  width: 90%;
                  text-align: center;
                  border: none;
                  color: var(--bs-gray);
                " />
          <span class="input-group-append">
            <button class="btn btn-primary rounded-pill ms-n5" type="button" style="
                    margin-left: -60px;
                    height: 55px;
                    width: 55px;
                    margin-top: 0.4em;
                    border: none;
                  ">
              <i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="mb-5" style="height: auto; background: var(--bs-white); margin-top: 5rem">
  <div class="container-xl">
    <div class="row mt-5">
      <div class="col-md-3 offset-md-1">
        <div class="row mb-4">
          <span style="font-size: 16pt; font-weight: 600">AVAILABLE JOBS</span>
        </div>
        <div class="row">
          <span style="font-weight: 600">Top 5 Locations</span>
          <ul style="margin-left: 10px; list-style-type: none" id="list_city_name">
            
          </ul>
        </div>
      </div>
      
      <div class="col-md-8" >
        {{-- @include('listvacany') --}}
        <div class="row mb-5">
          <div class="col-md-12" id="list_vacancy">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <a class="btn btn-primary" href="https://jobseeker.software/jobs" type="button" style="width: 100%%; width: 90%">
              see all vacancies
            </a>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>
@endsection