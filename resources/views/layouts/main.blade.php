<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/953f807382.js" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
  <link rel="shortcut icon" href="./assets/jobseeker_white 1.svg">
  <script src="./js/new_candidate.js" defer></script>
  <title>Jobseeker | {{ $title }}</title>
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Inter:wght@200;300;400;500;600;700;800;900&display=swap');

    :root {
      --color_1: #e4007e;
      --color_2: #f545a6;
      --bs-font-sans-serif: 'Inter', sans-serif;
    }

    .btn-primary {
      color: var(--bs-white);
      background-color: var(--color_1);
      border-radius: 8px;
      border: none;
      width: 100px;
      font-weight: 600;
    }

    .btn-primary:hover {
      background-color: var(--color_2);
    }
  </style>
</head>

<body>
  <x-nav title="{{ $title }}" />

  @yield('container')

  <x-footer />
</body>
<script>

</script>
</html>