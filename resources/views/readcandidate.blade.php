<table class="table table-inverse table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Fullname</th>
            <th>Date of birth</th>
            <th>place of birth</th>
            <th>gender</th>
            <th>year experience</th>
            <th>last salary</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody id="candidate_list" name="candidate_list">
        @foreach ($candidate as $data)
        <tr id="cand_{{$data->candidate_id}}">
            <td>{{$data->candidate_id}}</td>
            <td>{{$data->full_name}}</td>
            <td>{{$data->dob}}</td>
            <td>{{$data->pob}}</td>
            <td>{{$data->gender}}</td>
            <td>{{$data->year_exp}}&nbsp;years</td>
            <td>@currency($data->last_salary)</td>
            <td><a onclick="edit_candidate({{$data->candidate_id}})"><i class="fa-solid fa-pen-to-square" style="color: var(--bs-green); cursor: pointer;" title="edit candidate" id="candidate_{{$data->candidate_id}}" data-bs-toggle="modal" data-bs-target="#formEditmodal"></i></a>
                &nbsp;<a onclick="deleteCandidate({{$data->candidate_id}})"><i class="fa-solid fa-trash-can" style="color: var(--bs-red); cursor: pointer;" title="delete candidate"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="row gx-0">
    Showing {{ $candidate->firstItem() }} - {{ $candidate->lastItem() }} of {{ $candidate->total() }} entires<br/><br/>
    {!! $candidate->links() !!}
</div> 
