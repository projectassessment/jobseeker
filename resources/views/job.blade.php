@extends('layouts.main')

@section('container')
    <section style="margin-top: 8rem; height: 100vh;">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        @foreach ($job as $job)
                            <x-listJob jobTitle="{{ $job->job_title }}" 
                                salary="{{ $job->salary }}" 
                                level="{{ $job->level }}"
                                country="{{ $job->country }}"
                                role="{{ $job->role }}"
                                />
                        @endforeach
                    </div>
                    <div class="col-md-6">
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
