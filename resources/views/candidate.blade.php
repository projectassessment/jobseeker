@extends('layouts.main')

@section('container')
<section style="margin-top: 8rem; height: 100vh; overflow-y: auto">
    <div class="container">
        <div class="d-flex bd-highlight mb-4">
            <div class="p-2 w-100 bd-highlight">
                <h2>Candidate</h2>
            </div>
            <div class="p-2 flex-shrink-0 bd-highlight">
                <button class="btn btn-success" id="btn-add" data-bs-toggle="modal" data-bs-target="#formModal">
                    <i class="fa-solid fa-plus"></i> Candidate
                </button>
            </div>
        </div>
        <div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control" id="search_candidate" placeholder="Search candidate">
              </div>
            <div id="list_table_candidate">
                @include('readcandidate')
            </div>          
            <div class="modal fade" id="formModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="formModalLabel">New Candidate</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form id="new_candidate" name="new_candidate" class="form-horizontal" novalidate="">
                                @method('post')
                                @csrf
                                <div class="form-group mb-2">
                                    <label>Full name</label>
                                    <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Alex" value="">
                                </div>
                                <div class="form-group date mb-2 " id="datepicker">
                                    <label>Date of birth</label>
                                    <input type="text" class="form-control datepicker" id="dob" style="cursor: pointer;" />
                                    <span class="input-group-append">
                                    </span>
                                </div>
                                <div class="form-group mb-2">
                                    <label>Place of birth</label>
                                    <input type="text" class="form-control" id="pob" name="pob" placeholder="Jakarta" value="">
                                </div>
                                <div class="form-group mb-2">
                                    <label>Gender</label>
                                    <select class="form-group form-select" aria-label="Default select example" id="gender">
                                        <option selected value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>

                                <div class="form-group mb-2">
                                    <label>Years of experience</label>
                                    <input type="number" class="form-control" id="year_exp" name="year_exp  " placeholder="5" value="">
                                </div>
                                <div class="form-group mb-2">
                                    <label>Last Salary</label>
                                    <input type="number" class="form-control" id="last_salary" name="last_salary" placeholder="5.000.000" value="">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add" style="width: 30%">Save changes
                            </button>
                            <input type="hidden" id="candidate_id" name="candidate_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="formEditmodal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="formModalLabel">Edit Candidate</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form id="new_candidate_edit" name="new_candidate_edit" class="form-horizontal" novalidate="">
                                @method('post');
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection