<footer style="background: #202020; margin-top: 10rem;">
    <div class="container-xl">
      <div class="footer-content" style="padding: 30px 0px 0px 0px">
        <div class="row">
          <div class="col-md-12 mb-3">
            <img
              src="./assets/jobseeker_white 1.svg"
              style=""
              class="img-fluid"
              alt=""
              onclick="window.location = 'landing'"
            />
          </div>
          <div class="col-md-6 mt-3 mb-3">
            <div class="row">
              <div class="col-md-12 mb-3">
                <span style="color: var(--bs-white); font-weight: 700"
                  >INDONESIA</span
                >
                <p class="mb-1" style="color: var(--bs-white)">Jakarta</p>
                <p
                  class="mb-1 footer-town-addr"
                  style="color: var(--bs-white)"
                >
                  Jakarta AD Premier Office Park, 9th Floor<br />
                  Jl. TB Simatupang No.5, Ragunan, Pasar Minggu<br />
                  South Jakarta City, Jakarta 12550
                </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 mb-3">
                <p class="mb-1" style="color: var(--bs-white)">Bali</p>
                <p
                  class="mb-1 footer-town-addr"
                  style="color: var(--bs-white)"
                >
                  jl. Karang Mas, Jimbaran, South Kuta<br />
                  Kabupaten Badung, Bali, 80361<br />
                </p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 mb-3">
                <p
                  class="mb-1"
                  style="color: var(--bs-white); font-weight: 700"
                >
                  SINGAPORE
                </p>
                <p
                  class="mb-1 footer-town-addr"
                  style="color: var(--bs-white)"
                >
                  10 Anson Road #22-02 International Plaza,<br />
                  Singapore, 079903<br />
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-6 mt-3 mb-3">
            <div class="row">
              <div class="col-md-4 mb-1">

              </div>
              <div class="col-md-4 mb-1">
                  <p class="mb-3 footer-jobseeker" style="text-transform: uppercase; color: var(--bs-white); font-weight: 600;">For Candidate</p>
                <a style="text-decoration: none; color: var(--bs-white);"
                  href="#"
                >
                  <p class="mb-2 footer-jobseeker-text" >jobseeker.partners</p>
                </a>
                <a style="text-decoration: none; color: var(--bs-white);"
                href="#"
              >
                <p class="mb-2 footer-jobseeker-text">jobseeker.services</p>
              </a>
              <a style="font-weight: bold; color: var(--bs-white);"
              href="#"
            >
              <p class="mb-2 footer-jobseeker-text">Jobseeker App</p>
            </a>
              </div>
              <div class="col-md-4 mb-1">
                <p class="mb-3 footer-jobseeker" style="text-transform: uppercase; color: var(--bs-white); font-weight: 600;">For Candidate</p>
                <a style="text-decoration: none;"
                  href="#"
                >
                  <p
                    class="mb-2 footer-jobseeker-text"
                    style="color: var(--bs-white);"
                  >
                  jobseeker.app
                  </p>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-6 mt-4">
            <a
              href="mailto:info@jobseeker.company"
              style="text-decoration: none"
            >
              <p class="footer-town-addr mb-1" style="color: var(--bs-white)">
                <i class="fa-regular fa-envelope"></i> info@jobseeker.company
              </p>
            </a>
            <a
              style="text-decoration: none"
              href="https://api.whatsapp.com/send?text=Hello&amp;phone=+6281318817887"
            >
              <p
                class="footer-town-addr mb-1 pt-3"
                style="color: var(--bs-white)"
              >
                <i class="fa-brands fa-whatsapp"></i> +62 813 1881 7887
              </p>
            </a>
          </div>
          <div class="col-md-12 mt-3 text-center text-md-start">
            <a href="https://www.facebook.com/jobseekercompany"
              ><i class="fa-brands fa-facebook-f" style="color: var(--bs-white)"></i></a
            >&emsp;
            <a href="https://www.instagram.com/jobseekercompany/"
              ><i class="fa-brands fa-instagram" style="color: var(--bs-white)"></i></i></a
            >&emsp;
            <a href="https://twitter.com/jobseekerapp"
              ><i class="fa-brands fa-twitter" style="color: var(--bs-white)"></i></i></a
            >&emsp;
            <a href="https://www.linkedin.com/company/jobseeker-company/"
              ><i class="fa-brands fa-linkedin" style="color: var(--bs-white)"></i></a
            >&emsp;
            <a href="https://www.tiktok.com/@jobseekercompany"
              ><i class="fa-brands fa-tiktok" style="color: var(--bs-white)"></i></a
            >&emsp;
            <a href="https://t.me/jobseekercompany"
              ><i class="fa-brands fa-telegram" style="color: var(--bs-white)"></i></a
            >&emsp;
            <a href="https://www.youtube.com/channel/UCienG47UYFaRCp9J_j4uGwA"
              ><i class="fa-brands fa-youtube" style="color: var(--bs-white)"></i></a
            >&emsp;
          </div>
          <div class="col-md-12 border-bottom mt-md-5 mt-3 mb-3"></div>
          <div class="col-md-6 mt-1 mb-0 text-center text-md-start">
            <span class="float-left">
              <p class="footer-town-addr" style="color: var(--bs-white)">
                  Copyright © 2022 JobSeekerApps Pte. Ltd
              </p>
            </span>
          </div>
          <div class="col-md-6 mt-md-1 mt-0 mb-3 text-center text-md-end">
            <a  style="color: var(--bs-white);"
              href="https://jobseeker.company/privacy-policy"
              target="_blank"
            >
              <span> Privacy Policy </span>
            </a>
            &emsp;
            <a  style="color: var(--bs-white);"
              href="https://jobseeker.company/term-of-service"
              target="_blank"
            >
              <span> Terms of Service </span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>