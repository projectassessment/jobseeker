<div class="modal fade" id="formModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="formModalLabel">New Candidate</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="new_candidate" name="new_candidate" class="form-horizontal" novalidate="">
                    <div class="form-group mb-2">
                        <label>Full name</label>
                        <input type="text" class="form-control" id="full_name" name="full_name"
                                placeholder="Alex" value="">
                    </div>
                    <div class="form-group date mb-2" id="datepicker">
                        <label>Date of birth</label>
                        <input type="text" class="form-control" id="dob"/>
                        <span class="input-group-append">
                        </span>
                    </div>
                    <div class="form-group mb-2">
                        <label>Place of birth</label>
                            <input type="text" class="form-control" id="pob" name="pob"
                                placeholder="Jakarta" value="">
                    </div>
                    <div class="form-group mb-2">
                        <label>Gender</label>
                        <select class="form-group form-select" aria-label="Default select example" id="gender">
                            <option selected value="Male">Male</option>
                            <option value="Female">Female</option>
                          </select>
                    </div>
                   
                    <div class="form-group mb-2">
                        <label>Years of experience</label>
                            <input type="number" class="form-control" id="year_exp" name="year_exp  "
                                placeholder="5" value="">
                    </div>
                    <div class="form-group mb-2">
                        <label>Last Salary</label>
                            <input type="number" class="form-control" id="last_salary" name="last_salary"
                                placeholder="5.000.000" value="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-save" value="add" style="width: 30%">Save changes
                </button>
                <input type="hidden" id="candidate_id" name="candidate_id" value="0">
            </div>
        </div>
    </div>
</div>