{{-- style="box-shadow: rgb(179, 179, 173) 0px 0px 10px 1px" --}}
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white" >
    <div class="container-xl container-fluid">
      <a class="navbar-brand" href="/">
        <img
          src="./assets//nav_logo.svg"
          class="img-fluid"
          style="width: 160px"
          alt="Jobseeker software"
        />
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link {{ $title == 'Home' ? 'active' : '' }}" style="font-weight: 600" href="/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ $title == 'Candidate' ? 'active' : '' }}" style="font-weight: 600" href="/candidate">Candidate</a>
          </li>
        </ul>
      </div>
      <form class="justify-content-flex-end">
        <button class="btn btn-primary" type="button">Login</button>
      </form>
    </div>
</nav>