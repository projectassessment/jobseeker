
<div class="row mb-5 border-bottom pb-3">
    <div class="col s12">
        <div class="row">
        <div class="col-md-8">
            <div class="row">
            <h3 style="font-weight: 700">{{ $jobTitle }}</h3>
            </div>
        </div>
        <div
            class="col-md-3 d-flex justify-content-end align-items-center"
        >
            <div class="row">
            <span style="font-weight: 600; color: #20a6fc"
                >{{ $country }}</span
            >
            </div>
        </div>
        </div>
        <div class="row mt-2">
        <div class="col-md-3">
            <div class="row mb-3">
            <span style="color: var(--bs-gray)">Requirements:</span>
            </div>
            <div class="row mb-3">
            <span style="color: var(--bs-gray)">Salary:</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row mb-3">
            <span style="color: var(--bs-gray); font-weight: 500"
                >{{ $role }}&nbsp;{{ $level }}</span
            >
            </div>
            <div class="row mb-3">
            <span style="color: var(--bs-gray); font-weight: 500"
                >@currency($salary)</span
            >
            </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-5">
            <div class="row">
            <span style="color: var(--bs-gray); font-style: italic"
                >Posted 10 mins ago</span
            >
            </div>
        </div>
        <div
            class="col-md-3 offset-md-3 d-flex justify-content-end align-items-center"
            style="text-align: right"
        >
            <button
            class="btn btn-primary"
            type="button"
            style="width: 13%; width: 90%"
            >
            Apply Now
            </button>
        </div>
        </div>
    </div>
</div>
