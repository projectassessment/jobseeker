@props(['value'])

<h4 class="modal-title" id="formModalLabel">{{ $value ?? $slot}}</h4>